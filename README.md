Technologies :

- Spring Boot
- H2 embedded database (located in the current user home directory)
- Liquibase to create the database


To Run :

- mvn clean package && java -jar target/tinyurl-1.0.0.jar 

To Access the web page :

- http://localhost:8080/