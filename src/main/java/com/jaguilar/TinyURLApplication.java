package com.jaguilar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Tiny URL Application.
 * 
 * @author jaguilar
 *
 */
@SpringBootApplication
public class TinyURLApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinyURLApplication.class, args);
	}
}
