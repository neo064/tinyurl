package com.jaguilar.controller;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.jaguilar.TinyURLApplication;
import com.jaguilar.exception.TinyURLNotFoundException;

/**
 * Manage exceptions in the {@link TinyURLApplication}.
 * 
 * @author jaguilar
 *
 */
@ControllerAdvice
public class ErrorController {


	/**
	 * 404 page when a tiny url is not stored in the database.
	 * @param throwable the error
	 * @param model the page model
	 * @return the page to display
	 */
    @ExceptionHandler(TinyURLNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String exception(final Throwable throwable, final Model model) {
        String errorMessage = (throwable != null ? throwable.getMessage() : "Unknown error");
        model.addAttribute("errorMessage", errorMessage);
        return "error";
    }

}
