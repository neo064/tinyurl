package com.jaguilar.controller;

import org.springframework.web.bind.annotation.RestController;

/**
 * TinyURLRestController when needed.
 * 
 * @author jaguilar
 *
 */
@RestController
public class TinyURLRestController {
	
}
