package com.jaguilar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jaguilar.TinyURLApplication;
import com.jaguilar.controller.form.FullUrlPost;
import com.jaguilar.controller.form.TinyUrlPost;
import com.jaguilar.service.IURLService;

/**
 * Controller of the {@link TinyURLApplication}.
 * 
 * @author jaguilar
 *
 */
@Controller
public class TinyURLViewController {

	@Autowired
	private IURLService urlService;

	/**
	 * The Home Page.
	 * @param model the page model.
	 * @return the page to display
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET) 
	public String root(Model model) { 
		model.addAttribute("fullUrlPost", new FullUrlPost()); 
		model.addAttribute("tinyUrlPost", new TinyUrlPost()); 
		return "index"; 
	}

	/**
	 * The Home Page.
	 * @param model the page model.
	 * @return the page to display
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET) 
	public String index(Model model) {
		return root(model);
	}

	/**
	 * The page called when submitting an URL to be shrinked.
	 * @param model the page model.
	 * @return the page to display
	 */
	@RequestMapping(value = "/tinyURL", method = RequestMethod.POST)
	public String fullURLToTiny(@Valid FullUrlPost fullUrlPost, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("fullUrlPost", fullUrlPost); 
			model.addAttribute("tinyUrlPost", new TinyUrlPost()); 
			return "index";
		}
		String fullURLToTiny = urlService.fullURLToTiny(fullUrlPost.getFullURL());
		model.addAttribute("fullUrlPost", fullUrlPost);
		model.addAttribute("genTinyURL", fullURLToTiny);
		model.addAttribute("tinyUrlPost", new TinyUrlPost()); 
		return "index";
	}

	/**
	 * The page called when submitting a tiny url to get its original value.
	 * @param model the page model.
	 * @return the page to display
	 */
	@RequestMapping(value = "/fullURL", method = RequestMethod.POST)
	public String tinyURLToFull(@Valid TinyUrlPost tinyUrlPost, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("fullUrlPost", new FullUrlPost()); 
			model.addAttribute("tinyUrlPost", tinyUrlPost); 
			return root(model);
		}
		String tinyURLToFull = urlService.tinyURLToFull(tinyUrlPost.getTinyURL());
		model.addAttribute("tinyUrlPost", tinyUrlPost);
		model.addAttribute("tinyURLToFull", tinyURLToFull);
		model.addAttribute("fullUrlPost", new FullUrlPost()); 
		return "index";
	}
}
