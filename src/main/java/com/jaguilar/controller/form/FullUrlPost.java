package com.jaguilar.controller.form;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Represents the form posted when an URL will be converted in tiny URL.
 * 
 * @author jaguilar
 *
 */
public class FullUrlPost {

	@Size(min =11, max = 2000)
	@Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
	private String fullURL;

	public String getFullURL() {
		return fullURL;
	}

	public void setFullURL(String fullURL) {
		this.fullURL = fullURL;
	}


}
