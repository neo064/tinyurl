package com.jaguilar.controller.form;

import javax.validation.constraints.Size;

/**
 * Represents the form posted when an URL will be retrieved from a tiny URL.
 * @author jaguilar
 *
 */
public class TinyUrlPost {

	@Size(min =11, max = 2000)
	private String tinyURL;

	public String getTinyURL() {
		return tinyURL;
	}

	public void setTinyURL(String tinyURL) {
		this.tinyURL = tinyURL;
	}
}
