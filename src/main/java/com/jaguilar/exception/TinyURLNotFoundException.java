package com.jaguilar.exception;

/**
 * Exception thrown when a tiny URL is not stored in the database.
 * 
 * @author jaguilar
 *
 */
public class TinyURLNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public TinyURLNotFoundException() {
		super("The tinyURL does not exists");
	}

}
