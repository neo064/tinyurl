package com.jaguilar.model;

import java.text.MessageFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;


/**
 * URL representation in the database.
 * 
 * @author jaguilar
 *
 */
@Entity
public class UrlEntity {
	
	public static final String FULL_URL = "fullURL";
	public static final String TINY_URL = "tinyURL";
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Lob
	private String fullURL;
	
	@Lob
	private String tinyURL;
	
	protected UrlEntity() {
		
	}
	
	public UrlEntity(String fullURL, String tinyURL) {
		this.fullURL = fullURL;
		this.tinyURL = tinyURL;
	}
	public UrlEntity(String fullURL) {
		this(fullURL, "");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullURL() {
		return fullURL;
	}

	public void setFullURL(String fullURL) {
		this.fullURL = fullURL;
	}

	public String getTinyURL() {
		return tinyURL;
	}

	public void setTinyURL(String tinyURL) {
		this.tinyURL = tinyURL;
	}
	
	@Override
	public String toString() {
		return MessageFormat.format("ID = {0}, fullUrl = {1}, tinyUrl = {2}", id, fullURL, tinyURL);
	}
}
