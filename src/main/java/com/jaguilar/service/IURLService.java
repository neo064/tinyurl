package com.jaguilar.service;

import com.jaguilar.exception.TinyURLNotFoundException;

/**
 * Service that converts URL : </br>
 * - URL to tiny URL </br>
 * - Tiny URL to Original URL
 * 
 * @author jaguilar
 *
 */
public interface IURLService {

	/**
	 * Converts and store an URL to a tiny URL.
	 * @param url the url to convert
	 * @return the converted value
	 */
	public String fullURLToTiny(String url);

	/**
	 * Converts (retrieve from the database) a tiny URL to its original form.
	 * @param url the tiny URL to convert
	 * @return the original value
	 * @throws TinyURLNotFoundException if the tiny URL is not stored in the database
	 */
	public String tinyURLToFull(String url) throws TinyURLNotFoundException;
}
