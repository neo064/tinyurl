package com.jaguilar.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jaguilar.exception.TinyURLNotFoundException;
import com.jaguilar.model.UrlEntity;
import com.jaguilar.service.dao.IUrlEntityCrudDao;
import com.jaguilar.utils.URLUtils;

@Service
public class URLServiceImpl implements IURLService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private IUrlEntityCrudDao urlEntityDao;

	@Override
	@Transactional
	public String fullURLToTiny(String url) {
		UrlEntity findByFullURL = urlEntityDao.findByFullURL(url);
		if (findByFullURL == null) {
			UrlEntity urlEntity = new UrlEntity(url);
			urlEntity = urlEntityDao.save(urlEntity);
			urlEntity.setTinyURL(URLUtils.fullURLToTiny(url, urlEntity.getId()));
			urlEntityDao.save(urlEntity);
			return urlEntity.getTinyURL();
		}
		return findByFullURL.getTinyURL();
	}

	@Override
	public String tinyURLToFull(String url) {
		UrlEntity findByTinyURL = urlEntityDao.findByTinyURL(url);
		if (findByTinyURL == null) {
			throw new TinyURLNotFoundException();
		} 
		
		return findByTinyURL.getFullURL();
	}

}
