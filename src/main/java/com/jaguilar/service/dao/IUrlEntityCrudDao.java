package com.jaguilar.service.dao;

import org.springframework.data.repository.CrudRepository;

import com.jaguilar.model.UrlEntity;

/**
 * {@link UrlEntity} dao. 
 * 
 * @author jaguilar
 *
 */
public interface IUrlEntityCrudDao extends CrudRepository<UrlEntity, Long> {

	/**
	 * Retrieve an URL by its original value.
	 * @param fullURL the URL original form value to retrieve
	 * @return {@link UrlEntity} if found, null otherwise
	 */
	UrlEntity findByFullURL(String fullURL);
	
	/**
	 * Retrieve an URL by its tiny value.
	 * @param tinyURL the URL tiny value to retrieve
	 * @return {@link UrlEntity} if found, null otherwise
	 */
	UrlEntity findByTinyURL(String tinyURL);
	
}
