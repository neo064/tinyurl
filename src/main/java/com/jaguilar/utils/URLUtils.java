package com.jaguilar.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Class Utils to manipulate URL.
 * 
 * @author jaguilar
 *
 */
public final class URLUtils {

	private static String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static int BASE = ALPHABET.length();

	/**
	 * Convert an id to a base62 encoded String value.
	 * @param id the id to convert
	 * @return the encoded value
	 */
	public static String idToStringBase62(int id) {
		StringBuilder result = new StringBuilder();

		int i = id;
		while (i > 0) {
			result.append(ALPHABET.charAt(i % BASE));
			i /= BASE;
		}
		return result.reverse().toString();
	}

	/**
	 * Convert a base62 encoded String value to int.
	 * @param url the url to convert
	 * @return the decoded value
	 */
	public static int urlToIntBase62(String url) {
		int num = 0;

		for (char c : url.toCharArray()) {
			num = num * BASE + ALPHABET.indexOf(c);
		}
		return num;

	}
	
	/**
	 * Convert an URL to a tiny URL
	 * @param url the URL to convert
	 * @param id the id in the database used in the encoding
	 * @return the tiny URL
	 */
	public static final String fullURLToTiny(String url, Long id) {
		String domain = URLUtils.extractDomain(url);
		if (StringUtils.equals(url, domain)) {
			return url;
		}
		String idToStringURL = URLUtils.idToStringBase62(id.intValue());
		return new StringBuilder(domain).append(idToStringURL).toString();
	}

	
	/**
	 * Removes the domain from an URL.</br>
	 * Example : input = https://www.google.fr/hello/world => output = hello/world
	 * @param url the url to remove the domain
	 * @return the url without the domain
	 */
	public static String removeDomain(String url) {
		return url.replaceFirst(extractDomain(url), "");
	}

	/**
	 * Retrieves the domain from an URL.</br>
	 * Example : input = https://www.google.fr/hello/world => output = https://www.google.fr/
	 * @param url the URL to get the domain
	 * @return the domain
	 */
	public static String extractDomain(String url) {
		Pattern compile = Pattern.compile("^(https?://[^/]+/)");
		Matcher matcher = compile.matcher(url);
		if (matcher.find()) {
			return matcher.group(0);
		}
		return url;
	}

}
