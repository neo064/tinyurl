package com.jaguilar.unit.service;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.jaguilar.exception.TinyURLNotFoundException;
import com.jaguilar.model.UrlEntity;
import com.jaguilar.service.IURLService;
import com.jaguilar.service.dao.IUrlEntityCrudDao;


/**
 * Test the {@link IURLService} implementation.
 * @author jaguilar
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class URLServiceTest {
	
	private static final String fullURL = "https://www.thisisatest.com/hello/world";
	private static final String tinyURL = "https://www.thisisatest.com/b";
	private static final String fakeTinyURL = "https://www.thisisafail.com/b";
	
	@Autowired
	private IURLService urlService;
	
	@MockBean
    private IUrlEntityCrudDao dao;
	
	@Before 
	public void init() {
		UrlEntity urlEntityPersisted = new UrlEntity(fullURL);
		urlEntityPersisted.setId(1L);
		BDDMockito.given(dao.save((UrlEntity) BDDMockito.notNull())).willReturn(urlEntityPersisted);
		
		UrlEntity completeURL = new UrlEntity(fullURL, tinyURL);
		BDDMockito.given(dao.findByTinyURL(tinyURL)).willReturn(completeURL);
	}
	
	
	
	@Test
	public void newURLConversion() {
		Assert.assertEquals(tinyURL, urlService.fullURLToTiny(fullURL));
	}
	@Test
	public void existingURLConversion() {
		BDDMockito.given(dao.findByFullURL(fullURL)).willReturn(new UrlEntity(fullURL, tinyURL));
		Assert.assertEquals(tinyURL, urlService.fullURLToTiny(fullURL));
	}
	
	@Test
	public void findExistingTinyURL() {
		Assert.assertEquals(fullURL, urlService.tinyURLToFull(tinyURL));
	}
	
	@Test(expected=TinyURLNotFoundException.class)
	public void unknownExistingTinyURL() {
		urlService.tinyURLToFull(fakeTinyURL);
	}

}
