package com.jaguilar.unit.utils;

import java.util.concurrent.ThreadLocalRandom;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jaguilar.utils.URLUtils;

public class TinyURLUtilsTest {

	private static String url = "";
	
	private static int randomNum = -1;
	
	private static final String URL_TEST_URL = "https://www.mkyong.com/spring-boot/spring-boot-hello-world-example-thymeleaf/";
	private static final String TINY_URL = "https://www.mkyong.com/b";
	
	private static final String URL_TEST_END_URL_EXPECTED_RESULT = "spring-boot/spring-boot-hello-world-example-thymeleaf/";
	private static final String URL_TEST_DOMAIN_EXPECTED_RESULT = "https://www.mkyong.com/";
	
	
	
	@Before
	public void initData() {
		randomNum = ThreadLocalRandom.current().nextInt(0, 100000);
		url = URLUtils.idToStringBase62(randomNum);
	}
	
	@Test 
	public void urlToIntTest() {
		Assert.assertEquals(randomNum, URLUtils.urlToIntBase62(url));
	}
	
	@Test 
	public void decodeTest() {
		Assert.assertEquals(url, URLUtils.idToStringBase62(randomNum));
	}
	
	@Test
	public void convertURL() {
		Assert.assertEquals(TINY_URL, URLUtils.fullURLToTiny(URL_TEST_URL, 1L));
		Assert.assertEquals(URL_TEST_DOMAIN_EXPECTED_RESULT, URLUtils.fullURLToTiny(URL_TEST_DOMAIN_EXPECTED_RESULT, 1L));
	}
	
	@Test
	public void extractURL() {
		Assert.assertEquals(URLUtils.removeDomain(URL_TEST_URL), URL_TEST_END_URL_EXPECTED_RESULT);
		Assert.assertEquals(URLUtils.removeDomain(URL_TEST_DOMAIN_EXPECTED_RESULT), "");
	}
	
	@Test
	public void extractDomain() {
		Assert.assertEquals(URLUtils.extractDomain(URL_TEST_URL), URL_TEST_DOMAIN_EXPECTED_RESULT);
		Assert.assertEquals(URLUtils.extractDomain(URL_TEST_DOMAIN_EXPECTED_RESULT), URL_TEST_DOMAIN_EXPECTED_RESULT);
		
	}
}
